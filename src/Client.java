import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.Date;

public class Client {
	public static void main (String[] args) {
		try {
			Socket cliente = new Socket("localhost", 9999);
			ObjectInputStream entrada = new ObjectInputStream(cliente.getInputStream());
//			Date data_atual = (Date) entrada.readObject();
			String text = (String) entrada.readObject();
//			System.out.println("Data recebida do servidor: " + data_atual.toString());
			System.out.println("Data recebida do servidor: " + text.toString());
			entrada.close();
			System.out.println("Conexao encerrada");
			
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	
}
