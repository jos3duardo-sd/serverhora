import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Server {
	public static void main (String[] args) {
		try {
//			instanciando o ServerSocket ouvindo a porta 9999
			ServerSocket servidor = new ServerSocket(9999);
			System.out.println("Servidor ouvindo a porta 9999");
			
			while(true) {
//				metodo accept() bloqueia a execu��o at� que o servidor receba um pedido de conex�o
				Socket cliente = servidor.accept();
				System.out.println("Clietne conectado: " + cliente.getInetAddress().getHostAddress());
				ObjectOutputStream saida = new ObjectOutputStream(cliente.getOutputStream());
					
				
				Date date = new Date();  // wherever you get this
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				String text = df.format(date);
				
				
				saida.flush();
				saida.writeObject(text);
				saida.close();
				cliente.close();
			}
			
			
			
			
		} catch (Exception e) {
			System.out.println("Erro: " + e.getMessage());
		}
	}

}
